function sliderInit() {
    var sliderok = $('.js-mainslider');
    if(sliderok.length > 0){
        sliderok.slick({
            arrows:false,
            dots:true,
            autoplay: true,
            autoplaySpeed: 3000,
            customPaging: function(slider, i) {
                var div = document.createElement('div');
                div.classList.add("custom-paging__button");
                if(i === 0) div.classList.add("active");
                div.setAttribute('data-index', i);
                div.addEventListener('click',function () {
                    sliderok.slick('slickGoTo',i);
                    $('.custom-paging__button').removeClass('active');
                    div.classList.add('active');
                });
                $(div).text('');
                sliderok.parent().find('.js-mainslider-paging')[0].appendChild(div);
            }
        });

        sliderok.on('beforeChange', function (event, slick, currentSlide) {
            var x = $('.custom-paging__button').length;
            var y = $('.custom-paging__button.active').length;
            if (x == y) {
                $('.custom-paging__button').removeClass('active');
            }
        });
        sliderok.on('afterChange', function (event, slick, currentSlide) {
            $('.custom-paging__button').removeClass('active');
            $('.custom-paging__button').eq(currentSlide).addClass('active');
        });
        $(document).on('click', '.js-mainslider-prev', function () {
            sliderok.slick('slickPrev');
        });
        $(document).on('click', '.js-mainslider-next', function () {
            sliderok.slick('slickNext');
        });
    }
    var anchorslider = $('.js-anchor-slider');
    if(anchorslider.length > 0){
        anchorslider.slick({
            arrows:true,
            dots:false,
            slidesToShow: 4,
            asNavFor: '.js-big-slider',
            focusOnSelect: true,
            infinite: true,
            responsive: [
                {  breakpoint: 992,  settings: { slidesToShow: 3  } },
            ]
        });

    }
    var bigslider = $('.js-big-slider');
    if(bigslider.length > 0){
        bigslider.slick({
            arrows:false,
            dots:false,
            slidesToShow: 1,
            asNavFor: '.js-anchor-slider',
            infinite: true,

        });
        $(document).on('click', '.js-bigslider-prev', function () {
            bigslider.slick('slickPrev');
        });
        $(document).on('click', '.js-bigslider-next', function () {
            bigslider.slick('slickNext');
        });
        bigslider.on('afterChange', function(event, slick, currentSlide){

            $('.js-big-slider-text').text((currentSlide + 1) + ' / '+ slick.slideCount);
        });
    }
}
function hideThis(elem){
    $(elem).closest('.topbanner').slideUp();
}
function dropdowns(){
    $(document).on('click', '.js-dd-btn', function(){ //for click dropdown
        var btn = $(this);
       var div = $(this).closest('.js-dd-box').find('.js-dd-hid');
       if(btn.hasClass('active')){
           btn.removeClass('active');
           div.stop().slideUp(100);
       }else{
           btn.addClass('active');
           div.stop().slideDown(100);
           var element = div.find('.js-red-scroll');
           if(element.length > 0 )
           if (element.length > 0)  {
               element.jScrollPane();
           }
       }

    });
    $(document).on('mouseenter mouseleave', '.js-hd-box', function(){ // for hover dropdown
        var btn = $(this).find('.js-hd-btn');
        if(btn.hasClass('active')){
            btn.removeClass('active');
        }else{
            btn.addClass('active');
        }

    });
    // $(document).on('mouseenter mouseleave', '.js-show-box', function(){
    //     var wrp = $(this).closest('.js-next-menu');
    //     var box = wrp.find('.js-pod-menu');
    //     if(box.hasClass('active')){
    //         box.removeClass('active');
    //     }else{
    //         box.addClass('active');
    //     }
    //
    // });

}
function matchStart(params, data) {
    params.term = params.term || '';
    if (data.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
        return data;
    }
    return false;
}

function select2init() {
    var elem = $('.js-select2-init');
    if(elem.length > 0){
        elem.select2({
            placeholder: 'Пошук',
            width: 'style'
        });
    }
    var elem2 = $('.js-select2');

    if(elem2.length > 0){
        elem2.select2({
            width: 'style',
            matcher: function(params, data) {
                return matchStart(params, data);
            },
        });
    }

    var elem3 = $('.js-select2-simple');
    if(elem3.length > 0){
        elem3.select2({
            width: 'style',
            minimumResultsForSearch: -1
        });
    }
}
function stylerInit() {
    var elem = $('.js-styler');
    if(elem.length > 0){
        elem.styler({
            selectSmartPositioning: false
        });
    }
}
function searchfieldResultShow() {
    $('.js-show-result').focus(function () {
        $(this).closest('.searchfield').find('.searchfield__result').stop().slideDown();
    });
    $('.js-show-result').blur(function () {
        $(this).closest('.searchfield').find('.searchfield__result').stop().slideUp();
    });
}
function sliderRange() {
    var elem = $('.ofcntr');
    if(elem.length > 0){
        elem.each(function () {
            var slider = $(this).find('.js-slider-range');
            var min = $(this).find('.js-slider-range-min');
            var max = $(this).find('.js-slider-range-max');
            slider.slider({
                range: true,
                min: slider.data('min'),
                max: slider.data('max'),
                values: [ slider.data('start'), slider.data('end') ],
                slide: function( event, ui ) {
                    min.val( ui.values[0]);
                    max.val( ui.values[1]);
                }
            });
        })
    }
}
function hideout(el) {
    if(el.closest('.catcard').hasClass('active')){
        el.closest('.catcard').removeClass('active');
    }else{
        $('.catcard').removeClass('active');
        el.closest('.catcard').addClass('active');
    }


}
function commonTabs() {
    $(document).on('click', '.js-tab-common-btn', function(){
        if(!$(this).hasClass('active')){
            var box = $(this).closest('.js-tab-common-box');
            var ind = $(this).index();
            var btns = box.find('.js-tab-common-btn');
            var mobBtns = box.find('.js-tab-common-btn-mobile');
            var containers = box.find('.js-tab-common-container');

            btns.removeClass('active');
            mobBtns.removeClass('active');
            containers.removeClass('active');

            $(this).addClass('active');
            containers.eq(ind).addClass('active').find('.js-tab-common-btn-mobile').addClass('active');
        }
    });
    $(document).on('click', '.js-tab-common-btn-mobile', function(){
        var box = $(this).closest('.js-tab-common-box');
        var ind = $(this).closest('.js-tab-common-container').index();
        var btns = box.find('.js-tab-common-btn');
        var containers = box.find('.js-tab-common-container');
        var mobBtns = box.find('.js-tab-common-btn-mobile');
        if(!$(this).hasClass('active')){

            btns.removeClass('active');
            mobBtns.removeClass('active');
            containers.removeClass('active');

            $(this).addClass('active');
            containers.eq(ind).addClass('active');
            btns.eq(ind).addClass('active');
        }else{
            btns.removeClass('active');
            mobBtns.removeClass('active');
            containers.removeClass('active');
        }
    });
}
function anchor(){
    $(document).on('click','a.anchor', function (e) {
        e.preventDefault();
        var scroller=$('html');
        var href = $(this).attr('href');
        var target = $(href).offset().top;
        $(scroller).animate({scrollTop:target},500);
    });

}
function formLogic() {
    $('.js-input-box').each(function(){
        var rel = $(this).find('.js-input-btn:checked').data('input-rel');
        $(this).find('.js-input-hid').addClass('hide').find('input').attr('disabled','disabled');
        $(this).find('.js-input-hid[data-input-rel='+rel+']').removeClass('hide').find('input').removeAttr('disabled');
    });
    $(document).on('change', '.js-input-btn', function () {
        var box = $(this).closest('.js-input-box');
        var rel = box.find('.js-input-btn:checked').data('input-rel');
        box.find('.js-input-hid').addClass('hide').find('input').attr('disabled','disabled');
        box.find('.js-input-hid[data-input-rel='+rel+']').removeClass('hide').find('input').removeAttr('disabled');
    });
}
function cartView() {
    $(document).on('change', '.cart__tumblr input[type=radio]', function () {
        if($(this).val() == 'mini'){
            $('.cart__table').addClass('mini');
        }else{
            $('.cart__table').removeClass('mini');
        }
    });
}
function showAnswerForm() {
    $(document).on('click', '.js-answer-popup', function (e) {
        e.preventDefault();
        $('.answer-form').remove();
        var popup = document.createElement('div');
        var html = $('#form-answer').clone();
        var y = $(this).offset().top;
        popup.classList.add('answer-form');
        $(popup).append(html).css('top',y);

        $('body').append(popup);
        return false;
    })
}
function goToTabs(e) {
    e.preventDefault();
    var ind = 2;
    if(!$('#anhor-tab .js-tab-common-btn').eq(ind).hasClass('active')){
        var box = $('#anhor-tab');
        var btns = box.find('.js-tab-common-btn');
        var mobBtns = box.find('.js-tab-common-btn-mobile');
        var containers = box.find('.js-tab-common-container');

        btns.removeClass('active');
        mobBtns.removeClass('active');
        containers.removeClass('active');

        btns.eq(ind).addClass('active');
        containers.eq(ind).addClass('active').find('.js-tab-common-btn-mobile').addClass('active');
    }
    var scroller=$('html');
    var target = $('#anhor-tab').offset().top;
    $(scroller).animate({scrollTop:target},500);
}
function filterApply(){
    $(document).on('change', '.filter input', function(){
        var y = $(this).offset().top;
        var x = $(this).offset().left;
        $('.filter-show').remove();
        var popup = document.createElement('div');
        popup.classList.add('filter-show');
        var html = "<button type='button'>показать 42</button> ";
        $(popup).append(html).css({'top':y-18, 'left': x + 200});
        $('body').append(popup);
    });
}
function showHidePass() {
    var selector = '.js-show-pass';
    if($(selector).length > 0){
        $(document).on('click', selector, handler);
    }else{
        $(document).off('click', selector, handler);
    }
    function handler() {
        var input =  $(this).closest('.popup__input').find('input');
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            input.attr('type', 'password');
        }else{
            $(this).addClass('active');
            input.attr('type', 'text');
        }
    }
}
$(document).ready(function () {
    sliderInit();
    dropdowns();
    showHidePass();
    select2init();
    filterApply();
    searchfieldResultShow();
    sliderRange();
    stylerInit();
    commonTabs();
    anchor();
    formLogic();
    cartView();
    showAnswerForm();
    Stickyfill.add($('.stickyfill__wrap'));
});
$(document).on('onComplete.fb', function( e, instance, current ) {

    current.$slide.find('.js-select2').select2({
        dropdownParent: current.$content,
        width:'style'
    });
    current.$slide.find('.js-select2-simple').select2({
        dropdownParent: current.$content,
        width:'style',
        minimumResultsForSearch: -1
    });

});