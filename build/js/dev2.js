function brandsSliderInit() {
    $('.js-mainbrands-slider').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        prevArrow: '<div class="banner__arrow banner__arrow--prev"></div>',
        nextArrow: '<div class="banner__arrow banner__arrow--next"></div>',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 800,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
}
function mainSortSlider(){

    var elem = $('.js-sort-slider');
    if(elem.length > 0){
        var tabs = $('.maintabs__tabs--mob');

        elem.on('init', function( ){
            var selector = '[data-sort='+$('.js-tab.active').attr('data-sort')+']';
            setTimeout(function () {
                elem.slick('slickFilter', selector);
            },100)
        });
        elem.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow: '<div class="banner__arrow banner__arrow--prev"></div>',
            nextArrow: '<div class="banner__arrow banner__arrow--next"></div>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 770,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                    }
                }
            ]

        });
        tabs.slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            infinity:false,
            prevArrow: '<div class="banner__arrow banner__arrow--prev red"></div>',
            nextArrow: '<div class="banner__arrow banner__arrow--next red"></div>',
            responsive: [
                {  breakpoint: 770, settings: {  slidesToShow: 1  } }
            ]

        });
        tabs.on('afterChange', function(event, slick, currentSlide){
            $('.js-tab').removeClass('active');
            $('.slick-current .js-tab').addClass('active');
            var selector = '[data-sort='+$('.js-tab.active').attr('data-sort')+']';
            setTimeout(function () {
                elem.slick('slickUnfilter');
                elem.slick('slickFilter', selector);
            },100)
        });
        $(document).on('click', '.js-bigslider-prev', function () {
            elem.slick('slickPrev');
        });
        $(document).on('click', '.js-bigslider-next', function () {
            elem.slick('slickNext');
        });
        elem.on('afterChange', function(event, slick, currentSlide){
            $('.js-big-slider-text').text((currentSlide + 1) + ' / '+ slick.slideCount);
        });
    }
    $(document).on('click' , '.js-tab',function () {
        if(!$(this).hasClass('active')){
            var box = $(this).closest('.js-tabs-box');
            var selector = '[data-sort='+$(this).attr('data-sort')+']';

            var btns = box.find('.js-tab');

            btns.removeClass('active');
            $(this).addClass('active');
            elem.slick('slickUnfilter');
            elem.slick('slickFilter', selector);
        }
    });
}
function watchedSlider(){
    var elem = $('.js-watched-slider');

    if(elem.length > 0) {
        elem.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow: '<div class="banner__arrow banner__arrow--prev"></div>',
            nextArrow: '<div class="banner__arrow banner__arrow--next"></div>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 770,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        arrows: false,
                    }
                }
            ]
        });
        $(document).on('click', '.js-watched-prev', function () {
            $(this).closest('.mainwatched__container').find('.js-watched-slider').slick('slickPrev');
        });
        $(document).on('click', '.js-watched-next', function () {
            $(this).closest('.mainwatched__container').find('.js-watched-slider').slick('slickNext');
        });
        elem.on('afterChange', function(event, slick, currentSlide){
            slick.$slider.closest('.mainwatched__container').find('.js-big-slider-text').text((currentSlide + 1) + ' / '+ slick.slideCount);
        });
    }
}
function perevagiSlider() {
    var elem = $('.js-perevagi-slider');
    if(elem.length > 0) {
        elem.slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            dots: false,
            arrows: true,
            prevArrow: '<div class="banner__arrow banner__arrow--prev"></div>',
            nextArrow: '<div class="banner__arrow banner__arrow--next"></div>',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 520,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

}
function productCount(item, count) {
    var x = parseInt(item.parent('.js-prod-counter').find('.js-count').val());
    if ((x <= 1) && (count === -1)) {
        item.parent('.js-prod-counter').find('.js-count').val(1);
    } else {
        item.parent('.js-prod-counter').find('.js-count').val(x + count);
    }
}
function likeIt() {
    $(document).on('click', '.js-like', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });
}
function showSeo() {
    $(document).on('click', '.js-mainseo-arrow', function () {
        var box = $(this).parent().find('.js-mainseo-box');
        var heigh = $(this).parent().find('.mainseo__text').outerHeight();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            box.removeClass('active');
            box.animate({'height': 105}, 300);
        } else {
            box.addClass('active');
            $(this).addClass('active');
            box.animate({'height': heigh}, 300);
        }
    })
}
function mobileNavigation() {
    var menu = $('.js-mobile-navigation');
    var button = $('.js-mobile-butter');
    $(document).on('click', '.js-mobile-butter', function (e) {
        e.stopPropagation();
        console.log('log');
        if (button.hasClass('active')) {
            button.removeClass('active');
            $('.mobilemenu').removeClass('active');
            menu.stop().slideUp('fast');
        } else {
            button.addClass('active');
            menu.slideDown(100);
            $('.mobilemenu').addClass('active');
        }
    });
    $(document).on('click touchstart',function (event){
        var menu = $('.js-mobile-navigation');
        if (!menu.is(event.target) && menu.has(event.target).length === 0 && !button.is(event.target) && button.has(event.target).length === 0) {
            button.removeClass('active');
            menu.stop().slideUp(100);
        }
    });

}
function uiSlider() {
    var elem = $('.js-slider-navigator');
    elem.slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        vertical: true,
        arrows: true,
        focusOnSelect: true,
        asNavFor: '.js-slider-rule',
        prevArrow: '<div class="banner__arrow banner__arrow--prev"></div>',
        nextArrow: '<div class="banner__arrow banner__arrow--next"></div>',
        responsive: [
            {  breakpoint: 600,  settings: { vertical: false, slidesToShow: 5,  } },
            {  breakpoint: 440,  settings: { vertical: false, slidesToShow: 4,  } },
            {  breakpoint: 370,  settings: { vertical: false, slidesToShow: 3,  } },
        ]
    });
    var elem2 = $('.js-slider-rule');
    elem2.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        asNavFor: '.js-slider-navigator'
    });
}
function uiSliderEmployee() {
    var elem = $('.js-employee-slider-navigator');
    elem.slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        vertical: true,
        arrows: true,
        focusOnSelect: true,
        centerMode: true,
        asNavFor: '.js-employee-slider-rule',
        prevArrow: '<div class="banner__arrow banner__arrow--prev"></div>',
        nextArrow: '<div class="banner__arrow banner__arrow--next"></div>',
    });
    var elem2 = $('.js-employee-slider-rule');
    elem2.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        asNavFor: '.js-employee-slider-navigator'
    });
}
function scrollers() {
    var element = $('.js-red-scroll');
    if (element.length > 0)  {
        element.jScrollPane();
    }
    if(window.innerWidth > 992){
        $('.cabmenu__list').removeAttr('style');
    }else{
        $('.catalog__items').removeClass('catalog__items--rowed');
        $('.cart__table').removeClass('mini');
    }
}

function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
var myEfficientFn = debounce(function() {  scrollers()}, 250);
window.addEventListener('resize', myEfficientFn);
function showSearch(){
    $(document).on('click', '.js-mobile-search', function (e) {
        e.stopPropagation();
        var button = $(this);
        var field = $('.js-mobile-search-field');
        e.preventDefault();
        if (button.hasClass('active')) {
            button.removeClass('active');
            field.stop().slideUp(100);
        } else {
            $('.mobilemenu').addClass('active');
            button.addClass('active');
            field.stop().slideDown(100);
        }
    });
    $(document).on('click touchstart',function (event){
        var button = $('.js-mobile-search');
        var field = $('.js-mobile-search-field');
        if (!field.is(event.target) && field.has(event.target).length === 0 && !button.is(event.target) && button.has(event.target).length === 0) {
            button.removeClass('active');
            field.stop().slideUp(100);
        }
    });
}
function showCatalog() {
    var btn = $('.js-catalog-btn');
    var list = $('.js-catalog-list');
    $(document).on('click', '.js-catalog-btn', function (event) {
        if(window.innerWidth <= 992){
            event.preventDefault();
            if (btn.hasClass('active')) {
                btn.removeClass('active');
                list.removeClass('active');
                $('body').css('position', 'initial');
            } else {

                btn.addClass('active');
                list.addClass('active');
                $('body').css('position', 'fixed');
            }
        }else{
           return true;
        }
    });
    $(document).on('click', '.js-close-catalog', function () {
        btn.removeClass('active');
        list.removeClass('active');
        $('.js-pod-menu').removeClass('active');
        $('body').css('position', 'initial');
    });
    $(document).on('click touchstart',function (event){
        if (!list.is(event.target) && list.has(event.target).length === 0 && !btn.is(event.target) && btn.has(event.target).length === 0) {
            btn.removeClass('active');
            list.removeClass('active');
            $('body').css('position', 'initial');
        }
    });
}
function secondMenu() {
    $(document).on('click', '.js-next-menu', function () {
        $(this).find('.js-pod-menu').addClass('active');
    });
    $(document).on('click', '.js-next-menu>a', function (e) {
        e.stopPropagation();
        return true;
    })
}
function hidePodMenu() {
    $(document).on('click', '.js-podmenu-btn', function (e) {
        e.stopPropagation();
        $('.js-pod-menu').removeClass('active');
    });
    $(document).on('click', '.js-podmenu-btn>a', function (e) {
        e.stopPropagation();
        return true;
    })
}
function phisOrNot() {
    $(document).on('click', '.js-phis-no', function () {
        if ($(this).hasClass('active')) {
            return;
        } else {
            $('.js-phis-no').removeClass('active');
            $(this).addClass('active');
        }
    });
}

function activateTag() {
    $(document).on('click', '.tag-a', function (e) {
        e.stopPropagation();
        e.preventDefault();
        var link = $(this);
        if (link.hasClass('active')) {
           link.removeClass('active');
        } else {
            link.addClass('active');
        }
    });
}
function printSrc() {
    var el = '[data-print-url]';
    if($(el).length > 0){
        $(document).on('click', el, function () {
            var url = $(this).attr('data-print-url');
            var frame = document.createElement('iframe');
            $(frame).hide().attr("src", url).appendTo("body");
            // frame.addEventListener('load', function () {
            //     $('body').addClass('hide-print');
            // });
        });
    }
}
$(document).ready(function () {
    printSrc();
    activateTag();
    phisOrNot();
    hidePodMenu();
    secondMenu();
    showCatalog();
    showSearch();
    scrollers();
    uiSliderEmployee();
    uiSlider();
    perevagiSlider();
    mobileNavigation();
    showSeo();
    brandsSliderInit();
    likeIt();
    mainSortSlider();
    watchedSlider();
    $(document).on('keypress', '.js-count', function (evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) return false;
        return true;
    })
});